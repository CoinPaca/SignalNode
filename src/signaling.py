from websockets.exceptions import ConnectionClosedError
from string import ascii_letters, digits
from random import choice
from json import dumps, loads


class SignalingServer:
	__connections = {}

	@staticmethod
	async def serve(websocket, path):
		id = ''.join(choice(ascii_letters + digits) for i in range(10))
		while id in SignalingServer.__connections:
			id = ''.join(choice(ascii_letters + digits) for i in range(10))
		data = dumps({"type": "wsid", "id": id})
		await websocket.send(data)
		SignalingServer.__connections[id] = websocket
		try:
			async for message in websocket:
				try:
					data = loads(message)
				except Exception:
					print("Invalid data", data)
				data["ws_sender"] = id
				await SignalingServer.process(data, id)
		except ConnectionClosedError:
			print("Closing onnection", id)
		finally:
			SignalingServer.__connections.pop(id, None)

	@staticmethod
	async def process(data, id):
		if data["type"] == "offer":
			await SignalingServer.offer(data, id)
		if data["type"] == "answer":
			await SignalingServer.answer(data)

	@staticmethod
	async def offer(data, id):
		keys = SignalingServer.__connections.keys()
		if len(keys) == 1:
			await SignalingServer.__connections[data["ws_sender"]].send(dumps({"type": "first"}))
			return
		key = choice([i for i in keys if i != id])
		await SignalingServer.__connections[key].send(dumps(data))

	@staticmethod
	async def answer(data):
		await SignalingServer.__connections[data["ws_target"]].send(dumps(data))
