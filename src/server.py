#!/usr/bin/env python3
from asyncio import get_event_loop
from websockets import serve
from signaling import SignalingServer

start_server = serve(SignalingServer.serve, "0.0.0.0", 23574)
get_event_loop().run_until_complete(start_server)
get_event_loop().run_forever()
